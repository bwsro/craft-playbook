var gulp          = require('gulp');
var sass          = require('gulp-sass');
var browserSync   = require('browser-sync');
var autoprefixer  = require('gulp-autoprefixer');
var plumber       = require('gulp-plumber');

gulp.task('browserSync', function () {
	browserSync.init({
		proxy: "playbook.l"
	});
});

gulp.task('sass', function () {
	gulp.src('src/sass/**/*.sass')
		.pipe(plumber())
		.pipe(sass())
		.pipe(autoprefixer())
		.pipe(gulp.dest('src/css'))
		.pipe(browserSync.reload({
			stream: true
		}));
});

gulp.task('watch', ['browserSync', 'sass'], function () {
	gulp.watch('src/sass/**/*.sass', ['sass']);
	gulp.watch('craft/templates/**/*.twig', browserSync.reload);
	gulp.watch('craft/templates/**/*.html', browserSync.reload);
	gulp.watch('src/js/**/*.js', browserSync.reload);
});