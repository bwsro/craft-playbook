<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(
	'*' => array(

	),

	'playbook.l' => array(
		'environmentVariables' => array(
			'baseUrl'  => 'http://www.playbook.l/',
		),
		 'siteUrl' => 'http://www.playbook.l/',
	),

	'playbook.bwdev.info' => array(
		'environmentVariables' => array(
			'baseUrl'  => 'http://playbook.bwdev.info/',
		),
		'siteUrl' => 'http://playbook.bwdev.info/',
	),

);
